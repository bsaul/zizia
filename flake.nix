{
  description = "Zizia";
  nixConfig = {
    bash-prompt = "λ ";
  };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    flake-utils.url = "github:numtide/flake-utils";
    nix-filter.url = "github:numtide/nix-filter";
    clethra.url = "sourcehut:~bradleysaul/clethra?ref=0.2.2";
  };

  outputs = { self, nixpkgs, flake-utils, nix-filter, clethra }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        project = "zizia";
        version = "0.3.0";
        filter = nix-filter.lib;

        agda-stdlib = pkgs.agdaPackages.standard-library;
        clethra-lib = clethra.packages.${system}.default;
        agda-pkgs = [
          agda-stdlib
          clethra-lib
        ];
        agda = pkgs.agda.withPackages
          (p: agda-pkgs);
      in
      {

        formatter = pkgs.nixpkgs-fmt;

        checks.whitespace = pkgs.stdenvNoCC.mkDerivation {
          name = "check-whitespace";
          dontBuild = true;
          src = ./.;
          doCheck = true;
          checkPhase = ''
            ${pkgs.haskellPackages.fix-whitespace}/bin/fix-whitespace --check
          '';
          installPhase = ''mkdir "$out"'';
        };

        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            agda
            pkgs.haskellPackages.fix-whitespace
          ];
        };

        packages.default = pkgs.agdaPackages.mkDerivation {
          pname = project;
          version = version;
          src = filter {
            root = ./.;
            include = [ "src" "zizia.agda-lib" ];
          };

          everythingFile = "./src/Everything.agda";

          buildInputs = agda-pkgs;

          meta = with pkgs.lib; {
            description = "Finite types";
            homepage = "https://gitlab.com/bsaul/zizia";
          };
        };
      });
}
