---
title: FinSet
---

<details>
<summary>Imports and Options</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}
module FinSet.Core where

open import Data.Nat using (ℕ)
open import Data.Product using (∃; _,_)
open import Level using (0ℓ)
open import Function using (_on_)
open import Relation.Binary
  using (REL ; Setoid; IsEquivalence)
open import Relation.Binary.PropositionalEquality
  using (_≡_ ; refl ; sym; trans; isEquivalence)
open import Relation.Binary.Construct.On as On using ()

open import Finite.Core using (StrictlyFinite)
```

</details>

```agda
FinSet : Set₁
FinSet = ∃ (StrictlyFinite {0ℓ} {0ℓ})
```

## Cardinality of a `FinSet`

The cardinality of a finite type is simply its associated `ℕ`.

```agda
∣_∣ : FinSet → ℕ
∣ (_ , n , _ ) ∣ = n
```

## Equivalence of `FinSet`

Equivalence of `FinSet` is defined as equivalence of cardinality.

```agda
_≈_ :  FinSet → FinSet → Set
_≈_ = _≡_ on ∣_∣

≈-isEquivalence : IsEquivalence _≈_
≈-isEquivalence = On.isEquivalence ∣_∣ isEquivalence

setoid : Setoid _ _
setoid = record { isEquivalence = ≈-isEquivalence}
```
