---
title: FinSet Base
---

<details>
<summary>Imports and Options</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}
module FinSet.Base where

open import Data.Product as × using (_,_)
open import FinSet.Core
  using (FinSet)
open import Finite.Base
  using (⊥̇ᶠ;  ⊤̇ᶠ; _+_; _*_)
```

</details>

## `FinSet` Algebra

```agda
𝟘 : FinSet
𝟘 = _ , ⊥̇ᶠ

𝟙 : FinSet
𝟙 = _ , ⊤̇ᶠ

infixr 1 _⊕_
_⊕_ : FinSet → FinSet → FinSet
_⊕_ = ×.zip _ _+_

infixr 2 _⊗_
_⊗_ : FinSet → FinSet → FinSet
_⊗_ = ×.zip _ _*_
```
