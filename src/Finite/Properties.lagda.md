---
title: Properties of Finite types
---

<details>
<summary>Options</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Data.Product using (_,_)
open import Level using (Level; _⊔_; 0ℓ)
open import Relation.Binary hiding (NonEmpty)
open import Function using (Inverse; flip; _∘_; _∘₂_)

open import Finite.Core
```

</details>

```agda
module Finite.Properties
  {a ℓ : Level}
  {A : Setoid a ℓ}
  (SF@(_ , A↔n) : StrictlyFinite A)
  where

open Setoid A renaming (_≈_ to _≈ᴬ_) hiding (refl)
open Inverse A↔n
```

<details>
<summary>Imports</summary>

```agda
open import Axiom.UniquenessOfIdentityProofs
open import Data.Fin as F using ()
open import Data.Fin.Properties as F using ()
open import Data.Nat as ℕ using ()
open import Function.Construct.Symmetry
  renaming (inverse to ↔sym)
  using ()
open import Function.Properties.Inverse using (Inverse⇒Injection)
open import Relation.Binary.PropositionalEquality
  using (refl)
```

</details>

## Predicates

```agda
NonEmpty : Set
NonEmpty = 0 ℕ.< ∣ SF ∣
```

```agda
NonTrivial : Set
NonTrivial = 1 ℕ.< ∣ SF ∣
```

## `Finite` types have orderings

### Order Relations

```agda
_≤_ : Carrier → Carrier → Set
a₁ ≤ a₂ = Inverse.from A↔n a₁ F.≤ Inverse.from A↔n a₂

_<_ : Carrier → Carrier → Set
a₁ < a₂ = Inverse.from A↔n a₁ F.< Inverse.from A↔n a₂

_>_ : Carrier → Carrier → Set
_>_ = flip _<_
```

### Bundles

```agda
≤-isPreorder : IsPreorder _≈ᴬ_ _≤_
≤-isPreorder = record
  { isEquivalence = isEquivalence
  ; reflexive = F.≤-reflexive ∘ from-cong
  ; trans = F.≤-trans
  }

≤-isPartialOrder : IsPartialOrder _≈ᴬ_ _≤_
≤-isPartialOrder = record
  { isPreorder = ≤-isPreorder
  ; antisym    =
      λ x y →
      trans
        (sym (inverseˡ  refl))
        (inverseˡ (F.≤-antisym x y))
  }

<-isStrictPartialOrder : IsStrictPartialOrder _≈ᴬ_ _<_
<-isStrictPartialOrder = record
  { isEquivalence = isEquivalence
  ; irrefl = F.<-irrefl ∘ from-cong
  ; trans = F.<-trans
  ; <-resp-≈ = (F.<-respʳ-≡ ∘ from-cong) , F.<-respˡ-≡ ∘ from-cong
  }

≤-isTotalOrder : IsTotalOrder _≈ᴬ_ _≤_
≤-isTotalOrder = record
  { isPartialOrder = ≤-isPartialOrder
  ; total          = λ x y → F.≤-total (from x) (from y)
  }

<-cmp : (x y : Carrier) → Tri (x < y) (x ≈ᴬ y) (x > y)
<-cmp x y with F.<-cmp (from x) (from y)
... | tri< x<y f g = tri< x<y (f ∘ from-cong) g
... | tri≈ f x≈y g =
  tri≈
    f
    (trans (trans (sym (strictlyInverseˡ _)) (to-cong x≈y)) (strictlyInverseˡ _))
    g
... | tri> f g x>y = tri> f (g ∘ from-cong) x>y

<-isStrictTotalOrder : IsStrictTotalOrder _≈ᴬ_ _<_
<-isStrictTotalOrder = record
  { isStrictPartialOrder = <-isStrictPartialOrder
  ; compare = <-cmp
  }
```

## `Finite` implies a `TotalOrder`

```agda
Finite⇒TotalOrder : TotalOrder a ℓ 0ℓ
Finite⇒TotalOrder = record { isTotalOrder = ≤-isTotalOrder }
```

## `Finite` implies a `StrictTotalOrder`

```agda
Finite⇒StrictTotalOrder : StrictTotalOrder a ℓ 0ℓ
Finite⇒StrictTotalOrder = record { isStrictTotalOrder = <-isStrictTotalOrder }
```

## `Finite` types have `DecidableEquality`

```agda
Finite⇒Decidable : Decidable _≈ᴬ_
Finite⇒Decidable = F.inj⇒≟ (Inverse⇒Injection (↔sym A↔n))
```

## `Finite` types are `DecSetoid`

```agda
Finite⇒DecSetoid : DecSetoid a ℓ
Finite⇒DecSetoid = F.inj⇒decSetoid (Inverse⇒Injection (↔sym A↔n))
```
