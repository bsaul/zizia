---
title: Permutations
---

<details>
<summary>Imports and Options</summary>

```agda
{-# OPTIONS --safe --without-K #-}
```

</details>

```agda
module Finite.Combinatorics.Permutation where
```

<details>
<summary>Imports</summary>

```agda
-- stdlib
open import Axiom.Extensionality.Propositional using (Extensionality)
open import Data.Fin using (Fin ; _≟_); open Fin
open import Data.Fin.Properties renaming (≡-setoid to Finₛ) using ()
open import Data.Nat as ℕ using (ℕ)
open import Data.Product as × using (_,_; _×_ ; Σ)
open import Data.Vec.Base as V using (tabulate ; _++_ ; Vec ; allFin); open Vec
open import Data.Vec.Relation.Unary.All as A using (All ; all? ; lookup)
open import Data.Vec.Membership.Propositional using (_∈_)
open import Data.Vec.Membership.Propositional.Properties
  using (∈-allPairs⁺ ; ∈-allFin⁺)
open import Function
  using (_∘_ ; Func; Injective; Congruent; Injection; _↣_; _↔_; mk↔ₛ′;  mk↣)
open import Function.Properties.Inverse using (↔-isEquivalence)
open import Level as L using (0ℓ)
open import Relation.Binary using (Setoid)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)
open import Relation.Nullary.Decidable using (_→-dec_ ; Dec ; map′)
open import Relation.Unary using (Decidable; Pred)
import Relation.Binary.Reasoning.Setoid as Reasoning

-- clethra
open import Predicate.Decidable as P using (Pred?)

-- zizia
open import Finite.Counting
open import Finite.Core using (fin)
open import Finite.Base using (_^_)
open import Finite.Propositional.Core using (StrictlyFinite)

private
  variable
     m n : ℕ

  ↔-setoid : Setoid (L.suc 0ℓ) 0ℓ
  ↔-setoid = record { isEquivalence = ↔-isEquivalence {ℓ = 0ℓ}}

  module ↔R = Reasoning ↔-setoid
```

</details>

The `DecInjective` type
gives a proof that injectivity of a function with finite domain and codomain
is `Decidable`.

```agda
DecInjective : Decidable (Injective {A = Fin m} {B = Fin n} _≡_ _≡_)
DecInjective f =
   map′
      (λ pxs → A.lookup pxs (∈-allPairs⁺ (∈-allFin⁺ _) (∈-allFin⁺ _)))
      (λ inj → A.universal (λ (x , y) → inj {x = x} {y = y}) (allPairs⁺ _))
      (all? (λ (x , y) → (f x ≟ f y) →-dec (x ≟ y)) (allPairs⁺ _))
   where allPairs⁺ : ∀ m → Vec (Fin m × Fin m) (m ℕ.* m)
         allPairs⁺ _ = V.allPairs (V.allFin _) (V.allFin _)
```

The `Injective?` type is a `Decidable` predicate on `Fin` to `Fin` functions.
The `FuncInjective?` type lifts the `Injective?` query to the `Func` type.

```agda
Injective? : (m n : ℕ) → Pred? (Fin m → Fin n) 0ℓ
Injective? m n = _ , DecInjective

FuncInjective? : (m n : ℕ) → Pred? (Func (Finₛ m) (Finₛ n)) 0ℓ
FuncInjective? m n = ×.map (_∘ Func.to) (_∘ Func.to) (Injective? m n)
```

```agda
module _ (ext : Extensionality 0ℓ 0ℓ) where

  _ℙ_ : (n m : ℕ) → ℕ
  n ℙ m = ∣_∣ ⦃ _^_ ext (fin n) (fin m) ⦄ (FuncInjective? m n)
```

The following test cases demonstrate that counting `FuncInjective?`
yields the permutation formula:

$$
\frac{n!}{(n - m)!}
$$

```agda
module tests (ext : Extensionality 0ℓ 0ℓ) where

  _Pᶠ_ : (n m : ℕ) → ℕ
  n Pᶠ m = _ℙ_ ext n m

  open import Data.Nat.Combinatorics
  -- TODO: prove _Pᶠ_ and _P_ equivalent

  _ : 2 Pᶠ 6 ≡ 0  ; _ = refl
  _ : 2 Pᶠ 0 ≡ 1  ; _ = refl
  _ : 4 Pᶠ 4 ≡ 24 ; _ = refl
  _ : 5 Pᶠ 2 ≡ 20 ; _ = refl
  _ : 6 Pᶠ 2 ≡ 30 ; _ = refl

  _ : 0 Pᶠ 1 ≡ 0 P 1    ; _ = refl
  _ : 0 Pᶠ 0 ≡ 0 P 0    ; _ = refl
  _ : 1 Pᶠ 1 ≡ 1 P 1    ; _ = refl
  _ : 3 Pᶠ 3 ≡ 3 P 3    ; _ = refl
  _ : 3 Pᶠ 2 ≡ 3 P 2    ; _ = refl
  _ : 3 Pᶠ 1 ≡ 3 P 1    ; _ = refl
  _ : 3 Pᶠ 0 ≡ 3 P 0    ; _ = refl
  _ : 10 Pᶠ 0 ≡ 10 P 0  ; _ = refl
  _ : 10 Pᶠ 1 ≡ 10 P 1  ; _ = refl
```


```agda
SimplePermutation : (m n : ℕ) → Set
SimplePermutation m n = Fin m ↣ Fin n

-- WIP
-- Permutation : ∀ {A B : Set} → StrictlyFinite A → StrictlyFinite B → Set
-- Permutation (m , _) (n , _) = Injection {!   !} {!   !}

module _ (ext : Extensionality 0ℓ 0ℓ) where
  open import Relation.Nullary.Negation

  -- WIP
  -- ℙcountsPerm : Fin (_ℙ_ ext n m) ↔ Permutation m n
  -- ℙcountsPerm {n = n} {m = ℕ.zero}  = begin
  --   Fin ((ext ℙ n) 0)           ≡⟨⟩
  --   Fin 1                       ≈⟨ {!   !} ⟩
  --   Σ (Fin n → Fin 0) (λ f → Congruent _≡_ _≡_ f × Injective _≡_ _≡_ f) ≈⟨ {!   !} ⟩
  --   Injection (Finₛ 0) (Finₛ n) ∎
  --   where open ↔R
  -- ℙcountsPerm {n = ℕ.zero} {m = ℕ.suc m}  = begin
  --   Fin ((ext ℙ ℕ.zero) (ℕ.suc m))      ≡⟨⟩
  --   Fin 0                               ≈⟨ {!   !} ⟩
  --   Injection (Finₛ (ℕ.suc m)) (Finₛ 0) ∎
  --   where open ↔R
  -- ℙcountsPerm {n = ℕ.suc n} {m = ℕ.suc m} = begin
  --   Fin ((ext ℙ ℕ.suc n) (ℕ.suc m))            ≈⟨ {!   !} ⟩
  --   Injection (Finₛ (ℕ.suc m)) (Finₛ (ℕ.suc n)) ∎
  --   where open ↔R

  -- Permutationᶠ : StrictlyFinite (SimplePermutation m n)
  -- Permutationᶠ = _ , ℙcountsPerm

```

