---
title: Finite setoids
---

<details>
<summary>Imports and Options</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}
```

</details>

```agda
module Finite.Core where
```

<details>
<summary>Imports</summary>

```agda
-- stdlib
open import Data.Fin using (Fin)
open import Data.Fin.Properties renaming (≡-setoid to Finₛ)
open import Data.Nat as ℕ using (ℕ)
open import Data.Product using (∃; _,_)
open import Function using (_on_; _-⟨_⟩-_; flip)
open import Function.Construct.Identity using (↔-id)
open import Level using (Level; _⊔_; 0ℓ)
open import Relation.Binary using (Setoid; IsEquivalence)
open import Relation.Binary.PropositionalEquality as ≡ using (_≡_ )

-- zizia
open import Finite.Definitions

private
  variable
    a b ℓ : Level
    A : Setoid a ℓ
    B : Setoid b ℓ
```

</details>

This module defines the `StrictlyFinite` predicate
for setoids (types with an equivalence relation).
A finite type (proposition) is one with finitely many inhabitants (proofs).
Specifically,
a type is `StrictlyFinite` if
there exists a natural number `n`
such that a type `A` is in a bijective correspondence with
[`Fin n`](https://agda.github.io/agda-stdlib/Data.Fin.Base.html).

```agda
StrictlyFinite : Setoid a ℓ → Set (a ⊔ ℓ)
StrictlyFinite A = ∃ λ n → A ♯ n
```

## Cardinality of a `Finite` type

The cardinality of a finite type is simply its associated `ℕ`.

```agda
∣_∣ : StrictlyFinite A → ℕ
∣ (n , _ ) ∣ = n
```

## Equivalence of `StrictlyFinite`

Equivalence of `StrictlyFinite` is defined as equivalence of cardinality.

```agda
_≈_ : StrictlyFinite A → StrictlyFinite B → Set
_≈_ = ∣_∣ -⟨ _≡_ ⟩- ∣_∣

≈-isEquivalence : ∀ {A : Setoid a ℓ} → IsEquivalence {A = StrictlyFinite A} _≈_
≈-isEquivalence = record
  { refl = ≡.refl
  ; sym = ≡.sym
  ; trans = ≡.trans
  }

setoid : ∀ {A : Setoid a ℓ} → Setoid (a ⊔ ℓ) 0ℓ
setoid {A = A} = record { isEquivalence = ≈-isEquivalence {A = A} }
```

## Basic Utilities

Lift a natural number into `StrictlyFinite`.

```agda
fin : (n : ℕ) → StrictlyFinite (Finₛ n)
fin n = _ , ↔-id (Fin n)
```
