---
title: Basic Operations on Finite types
---

<details>
<summary>Options</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}
```

</details>

```agda
module Finite.Base where
```

<details>
<summary>Imports</summary>

```agda
open import Axiom.Extensionality.Propositional using (Extensionality)
open import Data.Empty.Polymorphic using (⊥)
open import Data.Fin using (Fin); open Fin
open import Data.Fin.Properties
  renaming (≡-setoid to Finₛ)
  using (0↔⊥ ; 1↔⊤; +↔⊎; *↔×; ^↔→)
open import Data.Nat as ℕ using (ℕ)
open import Data.Product using (_,_; _×_)
open import Data.Product.Setoid
  renaming (×ₛ-cong to _⊗_)
open import Data.Sum.Setoid
  renaming (⊎ₛ-cong to _⊕_)
open import Data.Unit.Polymorphic using (⊤; tt)
open import Function using (Inverse; flip; const)
open import Function.Construct.Composition
  renaming (inverse to _∘_)
open import Function.Construct.Symmetry
  renaming (inverse to sym)
open import Function.Setoid
  renaming (→ₛ-cong to _⟴_)
open import Level using (Level; _⊔_; lower; 0ℓ)
open import Relation.Binary
  using (Setoid)
open import Relation.Binary.PropositionalEquality
  using (_≡_ ; refl)
  renaming (setoid to ≡-setoid ; cong to ≡-cong)
open import Relation.Nullary.Negation
  using (contradiction)

open import Finite.Core

private
  variable
    a b ℓ : Level
    A : Setoid a ℓ
    B : Setoid b ℓ
```

</details>


## Operations and Identities

```agda
infixr 6 _+_
_+_ : StrictlyFinite A → StrictlyFinite B → StrictlyFinite (A ⊎ₛ B)
(_ , m↔I) + (_ , n↔J) = _ , (+↔⊎ ∘ sym ⊎-setoid↔≡) ∘ (m↔I ⊕ n↔J)

private
  ∣+∣ : ∀ {Aᶠ : StrictlyFinite A} → {Bᶠ : StrictlyFinite B}
      → ∣ Aᶠ + Bᶠ ∣ ≡ ∣ Aᶠ ∣ ℕ.+ ∣ Bᶠ ∣
  ∣+∣ = refl

infixr 7 _*_
_*_ : StrictlyFinite A → StrictlyFinite B → StrictlyFinite (A ×ₛ B)
(_ , m↔I) * (_ , n↔J) =  _ , (*↔× ∘ sym ×-setoid↔≡) ∘ (m↔I ⊗ n↔J)

private
  ∣*∣ : ∀ {Aᶠ : StrictlyFinite A} → {Bᶠ : StrictlyFinite B}
      → ∣ Aᶠ * Bᶠ ∣ ≡ ∣ Aᶠ ∣ ℕ.* ∣ Bᶠ ∣
  ∣*∣ = refl

module _ (ext : Extensionality 0ℓ 0ℓ) where

  infixr 7 _^_
  _^_ : StrictlyFinite A → StrictlyFinite B → StrictlyFinite (B →ₛ A)
  (_ , m↔I) ^ (_ , n↔J) = _ , ((^↔→ ext ∘ sym (→-setoid↔≡ ext)) ∘ (n↔J ⟴ m↔I))

  private
    ∣^∣ : ∀ {Aᶠ : StrictlyFinite A} → {Bᶠ : StrictlyFinite B}
        → ∣ Aᶠ ^ Bᶠ ∣ ≡ ∣ Aᶠ ∣ ℕ.^ ∣ Bᶠ ∣
    ∣^∣ = refl

⊥̇ᶠ : ∀ {ℓ} → StrictlyFinite (≡-setoid (⊥ {ℓ}))
⊥̇ᶠ =  0
    , record
      { to = λ ()
      ; from = λ ()
      ; to-cong = ≡-cong (λ ())
      ; from-cong = ≡-cong (λ ())
      ; inverse =
           (λ {x} y≡⊥ → contradiction y≡⊥ (const (lower x)))
          , λ {_} {y = y} y≡⊥ → contradiction y≡⊥ (const (lower y))
      }

private
  ∣⊥̇ᶠ∣ : ∀ {ℓ} → ⊥̇ᶠ {ℓ} ≈ fin 0
  ∣⊥̇ᶠ∣ = refl

⊤̇ᶠ : ∀ {ℓ} → StrictlyFinite (≡-setoid (⊤ {ℓ}))
⊤̇ᶠ = 1
    , record
      { to = λ _ → tt
      ; from = λ _ → zero
      ; to-cong = λ _ → refl
      ; from-cong = λ _ → refl
      ; inverse =
              (λ { _ → refl} )
            ,  λ { _ → Inverse.inverseʳ 1↔⊤ refl}
      }

private
  ∣⊤ᶠ∣ : ∀ {ℓ} → ⊤̇ᶠ {ℓ} ≈ fin 1
  ∣⊤ᶠ∣ = refl
```

