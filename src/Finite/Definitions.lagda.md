---
title: Definitions for finite types
---

<details>
<summary>Options</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}
```

</details>

```agda
module Finite.Definitions where
```

<details>
<summary>Imports</summary>

```agda
open import Data.Fin.Properties renaming (≡-setoid to Finₛ)
open import Data.Nat using (ℕ)
open import Function using (Inverse)
open import Level using (Level; _⊔_)
open import Relation.Binary using (Setoid ; REL)

private
  variable
    a ℓ : Level
```

</details>


```agda
_♯_ : REL (Setoid a ℓ) ℕ (a ⊔ ℓ)
A ♯ n = Inverse (Finₛ n) A
```
