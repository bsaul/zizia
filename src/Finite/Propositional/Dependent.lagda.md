---
title: Finite dependent types (Setoids with Propositional Equality)
---

<details>
<summary>Imports and Options</summary>

```agda
{-# OPTIONS --safe --without-K #-}
```

</details>

```agda
module Finite.Propositional.Dependent where
```

<details>
<summary>Imports</summary>

```agda
-- stdlib
open import Axiom.UniquenessOfIdentityProofs
open import Axiom.Extensionality.Propositional
  using
    ( Extensionality
    ; lower-extensionality)
open import Data.Empty
open import Data.Fin using (Fin; _↑ˡ_); open Fin
open import Data.Fin.Properties
open import Data.Nat as ℕ using (ℕ; _+_; _*_)
open import Data.Product as × using (_×_; Σ; ∃; _,_)
open import Data.Product.Algebra renaming (×-cong to _⊗_) using ()
open import Data.Product.Setoid
open import Data.Sum as ⊎
open import Data.Sum.Algebra renaming (⊎-cong to _⊕_) using ()
open import Data.Sum.Setoid
open import Data.Unit using (⊤)
open import Data.Vec.Functional using (foldr)
open import Function.Setoid
open import Function using (Inverse; _↔_; _∘_; _∘₂_)
open import Function.Construct.Composition renaming (inverse to _∘ᵢ_)
open import Function.Instances.Inverse
open import Function.Construct.Identity using (↔-id)
open import Function.Construct.Symmetry using (↔-sym)
open import Function.Properties.Inverse using (↔-isEquivalence)
open import Level as L using (Level; _⊔_; 0ℓ)
open import Relation.Binary using (Setoid; Decidable)
open import Relation.Binary.PropositionalEquality as ≡ using (_≡_)
open import Relation.Unary using (Pred; ∅; Universal)
import Relation.Binary.Reasoning.Setoid as Reasoning

-- zizia
open import Finite.Definitions
open import Finite.Propositional.Core using (StrictlyFinite)
open import Finite.Counting

-- clethra
open import Function.Instances.Inverse.WithoutK using (Fin1↔∃P; Finx↔ΠP)

private
  variable
    a b ℓ : Level
    A : Set a
    B : Set b
    n : ℕ

  ↔-setoid : Setoid (L.suc 0ℓ) 0ℓ
  ↔-setoid = record { isEquivalence = ↔-isEquivalence {ℓ = 0ℓ}}

  module ↔R = Reasoning ↔-setoid
```

</details>


```agda
open Inverse

private
  ∑′ : (≡.setoid A ♯ n) → (A → ℕ) → ℕ
  ∑′ #A f = ∑ (f ∘ to #A)

  Π′ : (≡.setoid A ♯ n) → (A → ℕ) → ℕ
  Π′ #A f = ∏ (f ∘ to #A)

#Σ₀ : ∀ {s : A → ℕ}
   → (#A : Fin n → A)
   → Fin (∑ (s ∘ #A) ) ↔ Σ (Fin n) (Fin ∘ s ∘ #A)
#Σ₀ {n = ℕ.zero} {s = s} #A = begin
  Fin (∑ (s ∘ #A))              ≡⟨⟩
  Fin 0                         ≈⟨ 0↔⊥ ⟩
  ⊥                             ≈⟨ ⊥↔Σ⊥ ⟩
  Σ ⊥ (Fin ∘ s ∘ #A ∘ from 0↔⊥) ≈⟨ ΣA↔ΣB (Decidable⇒UIP.≡-irrelevant _≟_) 0↔⊥ ⟨
  Σ (Fin 0) (Fin ∘ s ∘ #A)      ∎
  where open ↔R
#Σ₀ {n = ℕ.suc x} {s = s} #A = begin
  Fin (∑ (s ∘ #A))                             ≡⟨⟩
  Fin (s (#A zero) + ∑ {n = x} (s ∘ #A ∘ suc)) ≈⟨ +↔⊎ ⟩
  (Fin (s (#A zero)) ⊎ Fin (∑ (s ∘ #A ∘ suc))) ≈⟨ (Fin1↔∃P ⊕ #Σ₀ {s = s} (#A ∘ suc)) ⟩
  (Σ (Fin 1) (Fin ∘ s ∘ #A ∘ (_↑ˡ x)) ⊎ Σ (Fin x) (Fin ∘ s ∘ #A ∘ suc) ) ≈⟨ distr-⊎ ⟨
  Σ (Fin 1 ⊎ Fin x) (Fin ∘ s ∘ #A ∘ ⊎.[ _↑ˡ x , suc ]) ≈⟨ ΣA↔ΣB (Decidable⇒UIP.≡-irrelevant _≟_) +↔⊎ ⟨
  Σ (Fin (ℕ.suc x)) (Fin ∘ s ∘ #A)
  ∎
  where open ↔R

open import Relation.Nullary.Decidable using (yes; no)

-- TODO: where to put the following two lemmas?
A♯⇒Decidable≡ : Fin n ↔ A → Decidable {A = A} _≡_
A♯⇒Decidable≡ #A x y with from #A x ≟ from #A y
... | yes fromx≡fromy = yes (begin
  x                 ≡⟨ strictlyInverseˡ #A x ⟨
  to #A (from #A x) ≡⟨ ≡.cong (to #A) fromx≡fromy ⟩
  to #A (from #A y) ≡⟨ strictlyInverseˡ #A y ⟩
  y                 ∎)
  where open ≡.≡-Reasoning
... | no fromx≠fromy = no (fromx≠fromy ∘ (≡.cong (from #A)))

A♯⇒UIP : Fin n ↔ A → UIP A
A♯⇒UIP = Decidable⇒UIP.≡-irrelevant ∘ A♯⇒Decidable≡

-- TODO: Not seeing how to generalize from `A : Set` to `A : Set a`,
--       as ↔R (Inverse Reasoning) wants types at the same level.
#Σ : ∀ {A : Set} {s : A → ℕ} {P : A → Set}
      → (#A : Fin n ↔ A)
      → (∀ (x : A) → Fin (s x) ↔ P x)
      → Fin (∑′ #A s) ↔ ∃ P
#Σ {n = x} {A = A} {s = s} {P = P} #A #B = begin
  Fin (∑′ #A s)       ≡⟨⟩
  Fin (∑ (s ∘ to #A)) ≈⟨ #Σ₀ {s = s} (to #A) ⟩
  ∃ (Fin ∘ s ∘ to #A) ≈⟨ ∃-cong ( #B ∘ to #A ) ⟩
  ∃ (P ∘ to #A)       ≈⟨ ΣA↔ΣB′ (A♯⇒UIP #A) #A ⟩
  ∃ P                 ∎
  where open ↔R
```

## `StrictlyFinite` `Σ` type

```agda
Σᶠ : ∀ {A : Set} {s : A → ℕ} {P : A → Set}
  → StrictlyFinite A
  → (∀ (x : A) → Fin (s x) ↔ P x)
  → StrictlyFinite (Σ A P)
Σᶠ (_ , A↔m) F = _ , #Σ A↔m F
```


```agda
private
  Π : (A : Set a) → (Pred A ℓ) → Set _
  Π A B = ∀ (x : A) → B x

module _ (ext : Extensionality 0ℓ 0ℓ) where

  #Π₀ : ∀ {s : A → ℕ} (#A : Fin n → A) → Fin (∏ (s ∘ #A)) ↔ Π (Fin n) (Fin ∘ s ∘ #A)
  #Π₀ {n = ℕ.zero} {s = s} #A = begin
      Fin (∏ (s ∘ #A))
    ≡⟨⟩
      Fin 1
    ≈⟨ 1↔⊤ ⟩
      ⊤
    ≈⟨ ⊤↔Π⊥ ⟩
      Π ⊥ ∅
    ≈⟨ ΠA↔ΠB {extᵃ = ext} {extᵇ = ext} {uip = Decidable⇒UIP.≡-irrelevant _≟_} 0↔⊥ ⟨
      Π (Fin 0) (λ _ → ⊥)
    ≈⟨ Π-cong {ext = ext} (λ()) ⟩
      Π (Fin 0) (Fin ∘ s ∘ #A)
    ∎
    where open ↔R
  #Π₀ {n = ℕ.suc x} {s = s} #A = begin
      Fin (∏ (s ∘ #A))
    ≡⟨⟩
      Fin (s (#A zero) * ∏ {n = x} (s ∘ #A ∘ suc))
    ≈⟨ *↔× ⟩
      (Fin (s (#A zero)) × Fin (∏ {n = x} (s ∘ #A ∘ suc)))
    ≈⟨ ↔-id (Fin (s (#A zero))) ⊗ #Π₀ {s = s} (#A ∘ suc) ⟩
      (Fin (s (#A zero)) × Π[ (Fin ∘ s ∘ #A ∘ suc) ])
    ≈⟨ Finx↔ΠP {ext = ext} ⟩
      Π (Fin (ℕ.suc x)) (Fin ∘ s ∘ #A)
    ∎
    where open ↔R

  #Π : ∀ {A : Set} {s : A → ℕ} {P : A → Set}
        → (#A : Fin n ↔ A)
        → (∀ (x : A) → Fin (s x) ↔ P x)
        → Fin (Π′ #A s) ↔ Π A P
  #Π {n = x} {A = A} {s = s} {P = P} #A #P = begin
    Fin (Π′ #A s)               ≡⟨⟩
    Fin (∏ (s ∘ to #A))         ≈⟨ #Π₀ {s = s} (to #A) ⟩
    Π (Fin x) (Fin ∘ s ∘ to #A) ≈⟨ Π-cong {ext = ext} (#P ∘ to #A) ⟩
    Π (Fin x) (P ∘ to #A)       ≈⟨ ΠA↔ΠB {extᵃ = ext} {ext} {A♯⇒UIP #A} (↔-sym #A) ⟨
    Π A P                       ∎
    where open ↔R
```

## `StrictlyFinite` `Π` type

```agda
  Πᶠ : ∀ {A : Set} {s : A → ℕ} {P : A → Set}
      → StrictlyFinite A
      → (∀ (x : A) → Fin (s x) ↔ P x)
      → StrictlyFinite Π[ P ]
  Πᶠ (_ , A↔m) F = _ , #Π A↔m F
```
