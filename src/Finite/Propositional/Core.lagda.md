---
title: Finite types (Setoids with Propositional Equality)
---

<details>
<summary>Imports and Options</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}
```

</details>

```agda
module Finite.Propositional.Core where
```

<details>
<summary>Imports</summary>

```agda
-- stdlib
open import Axiom.Extensionality.Propositional
  using
    ( Extensionality
    ; lower-extensionality)
open import Data.Empty
open import Data.Product as ×
open import Data.Product.Setoid
open import Data.Sum as ⊎
open import Data.Sum.Setoid
open import Function.Setoid
open import Function using (Inverse; _↔_; _∘_; _∘₂_)
open import Function.Construct.Composition renaming (inverse to _∘ᵢ_)
open import Function.Instances.Inverse
open import Level as L using (Level; _⊔_; 0ℓ)
open import Relation.Binary using (Setoid)
open import Relation.Binary.PropositionalEquality as ≡ using ()

open import Finite.Definitions

private
  variable
    a b ℓ : Level
    A : Set a
    B : Set b
```

</details>

```agda
open import Finite.Core as FiniteSetoid using ()
open import Finite.Core using (∣_∣; _≈_ ; ≈-isEquivalence; setoid) public

StrictlyFinite : Set a → Set a
StrictlyFinite = FiniteSetoid.StrictlyFinite ∘ ≡.setoid
```

```agda
open import Finite.Base as F using ()
open import Finite.Base using (⊥̇ᶠ; ⊤̇ᶠ) public

infixr 6 _+_
_+_ : StrictlyFinite A → StrictlyFinite B → StrictlyFinite (A ⊎ B)
_+_ = ×.map₂ (_∘ᵢ ⊎-setoid↔≡) ∘₂ F._+_

infixr 7 _*_
_*_ : StrictlyFinite A → StrictlyFinite B → StrictlyFinite (A × B)
_*_ = ×.map₂ (_∘ᵢ ×-setoid↔≡) ∘₂ F._*_

module _ {a b} {A : Set a} {B : Set b} (ext : Extensionality b a) where

  infixr 7 _^_
  _^_ : StrictlyFinite A → StrictlyFinite B → StrictlyFinite (B → A)
  _^_ = ×.map₂ (_∘ᵢ →-setoid↔≡ ext) ∘₂ (F._^_ (lower-extensionality _ _ ext))
```
