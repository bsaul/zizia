---
title: Counting
---

<details>
<summary>Options</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}
```

</details>

```agda
module Finite.Counting where
```

<details>
<summary>Imports</summary>

```agda
-- stdlib
open import Data.Fin using (Fin)
open import Data.Bool
open import Data.Nat as ℕ using (ℕ; _+_; _*_)
open import Data.Product using (_,_)
open import Data.Sum using (inj₁ ; inj₂ ; _⊎_)
open import Data.Vec.Functional using (foldr)
open import Function using (_∘_; Inverse)
open import Level as L using (Level; _⊔_; 0ℓ)
open import Relation.Binary using (Setoid)
open import Relation.Nullary.Decidable
open import Relation.Nullary.Negation using (¬_)
open import Relation.Nullary.Reflects

-- clethra
open import Predicate.Decidable as P using (Pred?)

-- zizia
open import Finite.Core using (StrictlyFinite)

private
  variable
    a ℓ : Level
    A : Set a
    n : ℕ
```

</details>

```agda
-- finite sum
∑ = foldr ℕ._+_ 0

-- finite product
∏ = foldr ℕ._*_ 1
```

```agda
private
  -- toSum is in stdlib > 2.0
  -- NOTE: using toSum b/c pattern matching on Bool or Dec
  --       gives warning about not being cubical-compatible
  toSum : Dec A → A ⊎ ¬ A
  toSum ( true because  [p]) = inj₁ (invert  [p])
  toSum (false because [¬p]) = inj₂ (invert [¬p])

--- Decidable "indicator" function
∥_∥ : Pred? A ℓ → A → ℕ
∥ P? ∥ x with toSum (P.∣ P? ∣ x)
... | inj₁ _ = 1
... | inj₂ _ = 0

-- Count a Pred? over a vector
count : Pred? A ℓ → (Fin n → A) → ℕ
count P? xs = ∑ (∥ P? ∥ ∘ xs)
```

## Count a `Pred?` on a `StrictlyFinite` type

```agda
module _ {A : Setoid a ℓ} ⦃ (_ , A↔n) : StrictlyFinite A ⦄ where
  ∣_∣ : Pred? (Setoid.Carrier A) ℓ → ℕ
  ∣ P? ∣ = count P? (Inverse.to A↔n)
```
