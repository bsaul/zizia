---
title: Instances on Finite types
---

<details>
<summary>Options</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}
```

</details>

```agda
module Finite.Instances where
```

<details>
<summary>Options</summary>

```agda
open import Finite.Core
open import Finite.Properties
open import Level using (Level)
open import Relation.Binary using (Setoid)
open import Data.Nat
  renaming (NonTrivial to ℕonTrivial)

private
  variable
    a ℓ : Level
    A : Setoid a ℓ
```

</details>

## Instances from the predicates

```agda
module _ ⦃ s : StrictlyFinite A ⦄ ⦃ ne : NonEmpty s ⦄ where

  instance
    NonEmpty⇒nonZero : NonZero ∣ s ∣
    NonEmpty⇒nonZero = >-nonZero ne
```

```agda
module _ ⦃ s : StrictlyFinite A ⦄ ⦃ nt : NonTrivial s ⦄ where

  instance
    NonTrivial⇒nonTrivial : ℕonTrivial ∣ s ∣
    NonTrivial⇒nonTrivial = n>1⇒nonTrivial nt
```
