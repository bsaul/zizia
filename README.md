---
title: "Zizea: finite types in Agda"
languages: [adga, nix]
---

Playground for finite types in Agda.

## Related Work

* [Finite Decidable Setoids](https://nextjournal.com/zampino/finite-decidable-setoids#properties-of-finite-decidable-setoids)
* [Finite types in `unimath` library](https://unimath.github.io/agda-unimath/univalent-combinatorics.classical-finite-types.html)
* [Dependently Typed Programming with Finite Sets](https://firsov.ee/finset/finset.pdf)
* [FinSet in `cubical`](https://agda.github.io/cubical/Cubical.Data.FinSet.Base.html#861)
